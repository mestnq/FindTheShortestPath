﻿using System;
using System.IO;

namespace Algorithm
{
    public class WorkWithFile
    {
        public int[,] ReadFile()
        {
            string[] txt = File.ReadAllLines(@"../../../TheAdjacencyMatrixOfAGraph.txt");
            int[,] matrix = new int[txt.Length, txt.Length];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                string[] str = txt[i].Split(" ");
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (Convert.ToInt32(str[j]) >= 0)
                    {
                        matrix[i, j] = Convert.ToInt32(str[j]);
                    }
                    else
                    {
                        throw new Exception("Value can't be negative in alghoritm Dijkstra");
                    }
                }
            }

            return matrix;
        }
    }
}