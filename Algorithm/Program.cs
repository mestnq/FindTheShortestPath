﻿using System;

namespace Algorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] matrix = new WorkWithFile().ReadFile();
            if (matrix.GetLength(0) != matrix.GetLength(1))
            {
                throw new Exception("Matrix is wrong");
            }

            Console.WriteLine($"Напишите стартовую вершину от 0 до {matrix.GetLength(1) - 1}");
            int startTop = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Напишите конечную вершину от 0 до {matrix.GetLength(1) - 1}, исключая {startTop}");
            int endTop = Convert.ToInt32(Console.ReadLine());
            int shortPath = new DijkstrasAlgorithm().Make(matrix, startTop, endTop);
            Console.WriteLine("Короткий путь равен: " + shortPath);
        }
    }

    public class DijkstrasAlgorithm
    {
        public int Make(int[,] matrix, int startTop, int endTop)
        {
            int v = matrix.GetLength(0);
            int[] key = new int[v]; //для хранения значений ключей всех вершин
            bool[] set = new bool[v]; //для представления набора вершин, включенных
            for (int i = 0; i < v; i++) //Инициализируем
            {
                key[i] = int.MaxValue;
                set[i] = false;
            }

            key[startTop] = 0; //выбрали стартовую вершину - 0
            int top = startTop; //выбрали стартовую вершину
            int u = startTop;
            
            while (!set[endTop])
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (set[j] != true && matrix[u, j] != 0 && key[top] + matrix[u, j] < key[j])
                    {
                        key[j] = key[top] + matrix[u, j];
                    }
                }

                set[top] = true;
                u = MinKey(key, set, v);
                if (u != -1)
                {
                    set[u] = true;
                    top = u;
                }
            }

            return key[endTop];
        }

        private static int MinKey(int[] key, bool[] set, int v)
        {
            int min = int.MaxValue;
            int minIndex = -1;
            for (int i = 0; i < v; i++)
            {
                if (set[i] != true && key[i] < min)
                {
                    min = key[i];
                    minIndex = i;
                }
            }

            return minIndex;
        }
    }
}